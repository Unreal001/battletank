// Fill out your copyright notice in the Description page of Project Settings.

#include "TankAIController.h"



void ATankAIController::BeginPlay()
{
	Super::BeginPlay();
	
	
}

void ATankAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
 auto PlayerTank =Cast<ATank>(GetWorld()->GetFirstPlayerController()->GetPawn());
 auto TankAim =GetPawn()->FindComponentByClass<UTankAimingComponent>();

	if (ensure(PlayerTank))
	{
		MoveToActor(PlayerTank, AcceptanceRadius);
		
		TankAim->AimAt(PlayerTank->GetActorLocation());
	   if(TankAim->GetFiringStates()==EFiringStutes::Locked)
		TankAim->Fire();
   }
	
}

void ATankAIController::SetPawn(APawn * InPawn)
{
	Super::SetPawn(InPawn);
	if(InPawn)
	{
		auto possesedTank = Cast<ATank>(InPawn);
		if (!ensure(possesedTank))return;
		possesedTank->OnDeath.AddUniqueDynamic(this, &ATankAIController::OnDeathTank);
	}
}

void ATankAIController::OnDeathTank()
{
	
	UE_LOG(LogTemp, Warning, TEXT("I  FUCKED UP"));

	if (!ensure(GetPawn()))return;
	GetPawn()->DetachFromControllerPendingDestroy();
	
}


