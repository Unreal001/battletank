// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "TankBarrel.h"
#include "TankTurret.h"
#include "Projectile.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TankAimingComponent.generated.h"
 UENUM()
	 enum class EFiringStutes :uint8
 {
	 Reloading = 0 ,
	 Aiming,
	 Locked	 ,
	 OutofAmmo
 };
class UTankBarrel;
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BATTLETANK_API UTankAimingComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UTankAimingComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	
	void AimAt(FVector HitLocation);
	
	UFUNCTION(BlueprintCallable, Category = Setup)
	void Intialize(UTankBarrel * BarrelToSet, UTankTurret * TurretToSet);
	
	UFUNCTION(BlueprintCallable, Category = Firing)
		void Fire();
	EFiringStutes GetFiringStates()const;
	UFUNCTION(BlueprintCallable,category="Setup")
	int32 GetRoundsLeft()const;
private:
	UPROPERTY(EditAnywhere, Category = "Firing")
		float LaunchSpeed = 10000.f;
	UPROPERTY(EditAnywhere, Category = "Firing")
	float ReloadTime = 3.f;
	
	UPROPERTY(EditAnywhere, Category = "Setup")
		TSubclassOf <AProjectile> ProjectileBP;
	UPROPERTY(EditAnywhere, Category = "Setup")
	int32 LeftRounds = 3;
	double LastTimeFire = 0;
	FVector AimingDirection;
	UTankBarrel *Barrel=nullptr;
	UTankTurret *Turret = nullptr;
	void MoveBarrel(FVector AimDir);

	void MoveTurret(FVector AimDir);
	bool	IsBarrelMoving();
public:
	UPROPERTY(BlueprintReadOnly, category = "States")
		EFiringStutes FiringStutes = EFiringStutes::Reloading;
};
