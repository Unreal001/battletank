// Fill out your copyright notice in the Description page of Project Settings.

#include "Tank.h"

#include "TankBarrel.h"

// Sets default values
ATank::ATank()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}


// Called when the game starts or when spawned
void ATank::BeginPlay()
{
	Super::BeginPlay();
	CurrentHealth = StartHealth;
}
// Called every frame
void ATank::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
}

float ATank::TakeDamage(float DamageAmount, FDamageEvent const & DamageEvent, AController * EventInstigator, AActor * DamageCauser)
{
	UE_LOG(LogTemp, Warning, TEXT("DamageAmount = %f"), DamageAmount);
	 int32 Damagetpoints=FPlatformMath::RoundToInt(DamageAmount);
	 int32 Damagetoapply = FMath::Clamp(Damagetpoints, 0, CurrentHealth);
	  CurrentHealth -= DamageAmount;
	  if (CurrentHealth <= 0) {
		  OnDeath.Broadcast();
	  }
return DamageAmount;
}

// Called to bind functionality to input
void ATank::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

float ATank::GetCurrenHealth() const
{
	return float(CurrentHealth)/float(StartHealth);
}

