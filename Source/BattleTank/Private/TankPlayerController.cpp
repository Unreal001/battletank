// Fill out your copyright notice in the Description page of Project Settings.


#include "TankPlayerController.h"

void ATankPlayerController::BeginPlay()
{
	Super::BeginPlay();
	if (!ensure(GetPawn()))return;

	auto TankAim = GetPawn()->FindComponentByClass<UTankAimingComponent>();
	
	if (!ensure(TankAim))return;
	FoundTankAimingCoponent(TankAim);
}

void ATankPlayerController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
AimToCrossHair();
	
}

void ATankPlayerController::SetPawn(APawn * InPawn)
{	 

	Super::SetPawn(InPawn);
	if (InPawn)
	{
		auto possesedTank = Cast<ATank>(InPawn);
		if (!ensure(possesedTank))return;
		possesedTank->OnDeath.AddUniqueDynamic(this, &ATankPlayerController::OnDeathTank);
	}
}

void ATankPlayerController::OnDeathTank()
{
	UE_LOG(LogTemp, Warning, TEXT("I  FUCKED UP"));


	StartSpectatingOnly();
}

void ATankPlayerController::AimToCrossHair()
{
	if (!ensure(GetPawn()))return;
	
	auto TankAim = GetPawn()->FindComponentByClass<UTankAimingComponent>();
	
	if (!ensure(TankAim))return;
	
	FVector HitLocation;
	if (GetSightRayLocation(HitLocation))
	             TankAim->AimAt(HitLocation);
	
}

bool ATankPlayerController::GetSightRayLocation(FVector& HitLocation) const
{
	
	int32 VPXSIZE, VPYSIZE;
	GetViewportSize(VPXSIZE, VPYSIZE);
	auto ScreenLocation = FVector2D(XCrosshairPos*VPXSIZE, YCrosshairPos*VPYSIZE);
	FVector LookDirection;
	if (GetLookDirection(ScreenLocation, LookDirection)) 
	{
		GetLokkVectorHitLocation(HitLocation, LookDirection);
		return true;
	}
	return false;

	
	
}

bool ATankPlayerController::GetLookDirection(FVector2D ScreenCords, FVector & LooKDir) const
{
	FVector CamWorld;
	
	return	DeprojectScreenPositionToWorld(ScreenCords.X,ScreenCords.Y, CamWorld, LooKDir);
	 
}

bool ATankPlayerController::GetLokkVectorHitLocation(FVector & HitLocation, FVector LooKDir) const
{
	FHitResult FirstHit;
	FVector StartLocation = PlayerCameraManager->GetCameraLocation();
	
	FVector Endlocation = StartLocation +( LooKDir*LineTraceRange);

	
if (GetWorld()->LineTraceSingleByChannel(FirstHit, StartLocation, Endlocation, ECollisionChannel::ECC_Camera))
	{
		HitLocation=FirstHit.Location;
		return true;
	}
else 
{
	HitLocation = StartLocation + (LooKDir*LineTraceRange);
	return true;
}
}

