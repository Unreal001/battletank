////// Fill out your copyright notice in the Description page of Project Settings.

#include "TankMovementComponent.h"

void UTankMovementComponent::Intialize(UTrack *RTrackToSet,UTrack *LTrackToSet)
{
	RTrack = RTrackToSet;
	LTrack = LTrackToSet;
}

void UTankMovementComponent::MoveForward(float Throw)
{
	
	RTrack->SetTrack(Throw);
	LTrack->SetTrack(Throw);
}

void UTankMovementComponent::TurnRight(float Throw)
{
	
	RTrack->SetTrack(-Throw);
	LTrack->SetTrack(Throw);
	
}

void UTankMovementComponent::MoveBackward(float Throw)
{
	
	RTrack->SetTrack(-Throw);
	LTrack->SetTrack(-Throw);
}

void UTankMovementComponent::TurnLeft(float Throw)
{
	
	RTrack->SetTrack(Throw);
	LTrack->SetTrack(-Throw);

}

void UTankMovementComponent::RequestDirectMove(const FVector & MoveVelocity, bool bForceMaxSpeed)
{
	auto F = GetOwner()->GetActorForwardVector().GetSafeNormal();
	auto Df = MoveVelocity.GetSafeNormal();
	auto tt = FVector::CrossProduct(F, Df);
	auto t = tt.Z;

	float Throw = FVector::DotProduct(F,Df);
	if (Throw > 0)
		Throw = 1.f;
	else
		Throw = -1.f;
	
	//if (t > 0)
	//	t = 1.f;
//	else
//		t = -1.f;
	TurnRight(t);
	MoveForward(Throw);


//UE_LOG(LogTemp, Warning, TEXT(" DEV= %f "),Throw );
}
