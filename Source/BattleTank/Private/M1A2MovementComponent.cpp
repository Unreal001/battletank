// Fill out your copyright notice in the Description page of Project Settings.

#include "M1A2MovementComponent.h"

void UM1A2MovementComponent::Intialize(UTrack * TrackToSet)
{
	Track = TrackToSet;
}

void UM1A2MovementComponent::MoveForward(float Throw)
{

}

void UM1A2MovementComponent::TurnRight(float Throw)
{
}

void UM1A2MovementComponent::MoveBackward(float Throw)
{
}

void UM1A2MovementComponent::TurnLeft(float Throw)
{
}

void UM1A2MovementComponent::RequestDirectMove(const FVector & MoveVelocity, bool bForceMaxSpeed)
{
}
