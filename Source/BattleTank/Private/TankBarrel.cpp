// Fill out your copyright notice in the Description page of Project Settings.

#include "TankBarrel.h"

void UTankBarrel::Elevate(float RelativeSpeed)
{
	RelativeSpeed = FMath::Clamp<float>(RelativeSpeed, -1, 1);
	auto ElevationChange = RelativeSpeed*MaxDegreePerSec*(GetWorld()->GetDeltaSeconds());
	auto NewElevation = RelativeRotation.Pitch + ElevationChange;
auto Elev=	FMath::ClampAngle(NewElevation, MinElevationAngle, MAxElevationAngle);
	SetRelativeRotation(FRotator(Elev,0,0));
	
	
}
