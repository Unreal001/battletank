// Fill out your copyright notice in the Description page of Project Settings.

#include "Track.h"

UTrack::UTrack()
{
	PrimaryComponentTick.bCanEverTick = true;
	
}

void UTrack::BeginPlay()
{
	Super::BeginPlay();
OnComponentHit.AddDynamic(this,&UTrack::onHit);

}


void UTrack::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction * ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	
	

}

void UTrack::onHit(UPrimitiveComponent * HitComponent, AActor * OtherActor, UPrimitiveComponent *OtherComp, FVector NormalImpulse, const FHitResult &Hit)
{

	OnDrive();
	//UE_LOG(LogTemp, Warning, TEXT(" I hIT::::::: "));
	ApplySideForces();
	CurrentThrottle = 0;
}


void UTrack::ApplySideForces()
{
	auto Slippagespeed = FVector::DotProduct(GetRightVector(), GetComponentVelocity());
	auto DeltaTime = GetWorld()->GetDeltaSeconds();
	auto As = (-Slippagespeed / DeltaTime)*GetRightVector();
auto Tank = Cast<UStaticMeshComponent>(GetOwner()->GetRootComponent());
auto F = (Tank->GetMass()*	As) / 2;

	Tank->AddForce(F);
}





void UTrack::SetTrack(float throttle)
{
	//auto Time = GetWorld()->GetTimeSeconds();
	CurrentThrottle = FMath::Clamp<float>(CurrentThrottle + throttle, -10, 10);
	

}

void UTrack::OnDrive()
{
	auto ForceApplied = GetForwardVector()*TrackMaxdrivingForce*CurrentThrottle;
	auto ForceLocation = GetComponentLocation();
	auto Tankroot = Cast<UPrimitiveComponent>(GetOwner()->GetRootComponent());
	Tankroot->AddForceAtLocation(ForceApplied, ForceLocation);
}


