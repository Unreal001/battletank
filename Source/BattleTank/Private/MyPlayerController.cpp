// Fill out your copyright notice in the Description page of Project Settings.

#include "MyPlayerController.h"



void AMyPlayerController::BeginPlay()
{
	Super::BeginPlay();
	if (!ensure(GetPawn()))return;
	auto	tank = (ATank*)GetPawn();
	auto	 TankAim = tank->FindComponentByClass<UTankAimingComponent>();
	if (!ensure(TankAim))
		return;
	FoundTankAimingCoponent(TankAim);
}

void AMyPlayerController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);


	AimToCrossHair();

}

void AMyPlayerController::AimToCrossHair()
{
	if (!ensure(GetPawn()))return;
	auto	tank = (ATank*)GetPawn();
	auto	 TankAim = tank->FindComponentByClass<UTankAimingComponent>();
	if (!ensure(TankAim))
		return;
	FVector HitLocation;
	if (GetSightRayLocation(HitLocation))
	{

		TankAim->AimAt(HitLocation);
	}
}

bool AMyPlayerController::GetSightRayLocation(FVector& HitLocation) const
{

	int32 VPXSIZE, VPYSIZE;
	GetViewportSize(VPXSIZE, VPYSIZE);
	auto ScreenLocation = FVector2D(XCrosshairPos*VPXSIZE, YCrosshairPos*VPYSIZE);
	FVector LookDirection;
	if (GetLookDirection(ScreenLocation, LookDirection))
	{
		GetLokkVectorHitLocation(HitLocation, LookDirection);
		return true;
	}
	return false;



}

bool AMyPlayerController::GetLookDirection(FVector2D ScreenCords, FVector & LooKDir) const
{
	FVector CamWorld;

	return	DeprojectScreenPositionToWorld(ScreenCords.X, ScreenCords.Y, CamWorld, LooKDir);

}

bool AMyPlayerController::GetLokkVectorHitLocation(FVector & HitLocation, FVector LooKDir) const
{
	FHitResult FirstHit;
	FVector StartLocation = PlayerCameraManager->GetCameraLocation();

	FVector Endlocation = StartLocation + (LooKDir*LineTraceRange);


	if (GetWorld()->LineTraceSingleByChannel(FirstHit, StartLocation, Endlocation, ECollisionChannel::ECC_Visibility))
	{
		HitLocation = FirstHit.Location;
		return true;
	}
	else
	{
		HitLocation = StartLocation + (LooKDir*LineTraceRange);
		return true;
	}
}


