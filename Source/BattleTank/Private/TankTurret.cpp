// Fill out your copyright notice in the Description page of Project Settings.

#include "TankTurret.h"

void UTankTurret::Rotate(float RelativeSpeed)
{
RelativeSpeed = FMath::ClampAngle(RelativeSpeed, -1,1);
	auto AzimthChange = RelativeSpeed*MaxDegreePerSec*(GetWorld()->GetDeltaSeconds());
	auto NewAzim =(RelativeRotation.Yaw + AzimthChange);
	
	
	SetRelativeRotation(FRotator(0, NewAzim, 0));
}
