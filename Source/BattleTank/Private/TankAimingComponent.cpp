// Fill out your copyright notice in the Description page of Project Settings.

#include "TankAimingComponent.h"


// Sets default values for this component's properties
UTankAimingComponent::UTankAimingComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UTankAimingComponent::BeginPlay()
{
	Super::BeginPlay();
	LastTimeFire = FPlatformTime::Seconds();
	// ...
	
}
void UTankAimingComponent::Intialize(UTankBarrel * BarrelToSet, UTankTurret * TurretToSet)
{

	Barrel = BarrelToSet;
	Turret = TurretToSet;
}



// Called every frame
void UTankAimingComponent::TickComponent(float DeltaTime, ELevelTick TickType,
	FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (LeftRounds == 0) 

		FiringStutes = EFiringStutes::OutofAmmo;
	
	
		else if ((FPlatformTime::Seconds() - LastTimeFire) < ReloadTime)
		{
			FiringStutes = EFiringStutes::Reloading;
		}
		else if (IsBarrelMoving())
		{
			FiringStutes = EFiringStutes::Aiming;
		}
		else
		{
			FiringStutes = EFiringStutes::Locked;
		}


}

void UTankAimingComponent::AimAt(FVector WorldSpaceAim)
{
	if (!ensure(Barrel))return;
	FVector OutLunchVelocity=FVector(ForceInitToZero);
	FVector StartLocation = Barrel->GetSocketLocation(FName("Projectile"));
bool bHaveAimSol=	UGameplayStatics::SuggestProjectileVelocity(this,
		OutLunchVelocity, StartLocation, WorldSpaceAim,
		LaunchSpeed,
		false,
		0,0, 
		ESuggestProjVelocityTraceOption::DoNotTrace, 
		FCollisionResponseParams::DefaultResponseParam,
		TArray<AActor*>(),
		false);
if (bHaveAimSol)
{	   
	
 AimingDirection = OutLunchVelocity.GetSafeNormal();
MoveBarrel(AimingDirection);
	
} 
else
MoveBarrel(AimingDirection);

}

void UTankAimingComponent::Fire()
{
	if (FiringStutes != EFiringStutes::OutofAmmo) 
	{
		if (FiringStutes != EFiringStutes::Reloading)
		{
			if (!ensure(Barrel&&ProjectileBP))  return;
			auto projectile = GetWorld()->SpawnActor<AProjectile>(
				ProjectileBP,
				Barrel->GetSocketLocation(FName("Projectile")),
				Barrel->GetSocketRotation(FName("Projectile")));
			
			projectile->LunchProjectile(LaunchSpeed);

			LastTimeFire = FPlatformTime::Seconds();
			LeftRounds--;
		}
	
	}
}

EFiringStutes UTankAimingComponent::GetFiringStates() const
{

	return FiringStutes;
}

int32 UTankAimingComponent::GetRoundsLeft() const
{
	return LeftRounds;
}


void UTankAimingComponent::MoveBarrel(FVector AimDir)
{
	if (!ensure(Barrel&&Turret))return;
	auto BarrelRotator = Barrel->GetForwardVector().Rotation();
	auto AimRotation = AimDir.Rotation();
	auto DeltaRot =  AimRotation - BarrelRotator;
	Turret->Rotate(DeltaRot.Yaw);
	Barrel->Elevate(DeltaRot.Pitch);

}

void UTankAimingComponent::MoveTurret(FVector AimDir)
{
	
	
}

bool UTankAimingComponent::IsBarrelMoving()
{
	if(!ensure(Barrel))return false;
	auto BarrelForward = Barrel->GetForwardVector();
	return !BarrelForward.Equals(AimingDirection, 0.001f);
}

