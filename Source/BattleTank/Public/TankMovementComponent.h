// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "Track.h"
#include "CoreMinimal.h"
#include "GameFramework/NavMovementComponent.h"
#include "TankMovementComponent.generated.h"

/**
 * 
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class BATTLETANK_API UTankMovementComponent : public UNavMovementComponent
{
	GENERATED_BODY()
public:
	 UFUNCTION(BlueprintCallable,Category=Setup)
	void Intialize(UTrack *RTrackToSet,UTrack *LTrackToSet);

		UFUNCTION(BlueprintCallable)
		void MoveForward(float Throw);

		UFUNCTION(BlueprintCallable)
			void TurnRight(float Throw);
	
		UFUNCTION(BlueprintCallable)
			void MoveBackward(float Throw);

		UFUNCTION(BlueprintCallable)
			void TurnLeft(float Throw);

		virtual void RequestDirectMove(const FVector& MoveVelocity, bool bForceMaxSpeed);


private:
	UTrack *RTrack = nullptr;
	UTrack *LTrack = nullptr;
};
