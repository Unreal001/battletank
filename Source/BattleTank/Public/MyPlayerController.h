// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "Tank.h"
#include"TankAimingComponent.h"
#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "MyPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class BATTLETANK_API AMyPlayerController : public APlayerController
{
	GENERATED_BODY()
	
protected:
	virtual void BeginPlay() override;
	virtual	void Tick(float DeltaTime) override;

public:
	UFUNCTION(BlueprintImplementableEvent, Category = "Setup")
		void FoundTankAimingCoponent(UTankAimingComponent* TankAimRef);



private:

	UFUNCTION(BlueprintCallable)
		void AimToCrossHair();
	bool GetSightRayLocation(FVector &HitLocation)const;
	bool GetLookDirection(FVector2D ScreenCords, FVector& LooKDir)const;
	bool GetLokkVectorHitLocation(FVector& HitLocation, FVector LooKDir)const;
	UPROPERTY(EditAnyWhere)
		float XCrosshairPos = 0.5f;

	UPROPERTY(EditAnyWhere)
		float YCrosshairPos = 0.3333f;

	UPROPERTY(EditAnyWhere)
		float LineTraceRange = 5000000.f;
	
	
};
