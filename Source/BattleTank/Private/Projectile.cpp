// Fill out your copyright notice in the Description page of Project Settings.

#include "Projectile.h"


// Sets default values
AProjectile::AProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	
	CollisionMesh= CreateDefaultSubobject<UStaticMeshComponent>(FName("CollisionMesh"));
	SetRootComponent(CollisionMesh);
	CollisionMesh->SetNotifyRigidBodyCollision(true);
	CollisionMesh->SetVisibility(false);
	ExplosionForce= CreateDefaultSubobject<URadialForceComponent>(FName("ExplosionForce"));
	ExplosionForce->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	
	LaunchBlast=CreateDefaultSubobject<UParticleSystemComponent>(FName("LuanchBlast"));
   LaunchBlast->AttachToComponent(RootComponent,FAttachmentTransformRules::KeepRelativeTransform);

   ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(FName("Projectile Movement"));
ProjectileMovement->bAutoActivate =		false;

ImpactBlast = CreateDefaultSubobject<UParticleSystemComponent>(FName("ImpactBlast"));
ImpactBlast->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
ImpactBlast->bAutoActivate = false;
}

void AProjectile::OnHit(UPrimitiveComponent * HitComponent, AActor * OtherActor, UPrimitiveComponent * OtherComp, FVector NormalImpulse, const FHitResult & Hit)
{
	LaunchBlast->Deactivate();
	ImpactBlast->Activate();
	//ExplosionForce->FireImpulse();
	SetRootComponent(LaunchBlast);
	float dd = -25;
	const TArray<AActor*> ac;
	dd=UGameplayStatics::ApplyRadialDamage(this,ProjectileDamage, GetActorLocation(),ExplosionForce->Radius,UDamageType::StaticClass(),ac);
	UE_LOG(LogTemp, Error, TEXT("Damge = %d"), dd);
	CollisionMesh->DestroyComponent();

	FTimerHandle Timer;
	GetWorld()->GetTimerManager().SetTimer(Timer, this, &AProjectile::ProjectDestroy, DestroyTime);
	this->Destroy(true);
}

// Called when the game starts or when spawned
void AProjectile::BeginPlay()
{
	Super::BeginPlay();
	CollisionMesh->OnComponentHit.AddDynamic(this, &AProjectile::OnHit);
}



void AProjectile::ProjectDestroy()
{
	Destroy();
}

void AProjectile::LunchProjectile(float LaunchSpeed)
{
	
	ProjectileMovement->SetVelocityInLocalSpace(FVector::ForwardVector*LaunchSpeed);
	ProjectileMovement->CreatePhysicsState();
	ProjectileMovement->Activate(true);
}

