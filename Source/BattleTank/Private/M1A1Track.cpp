// Fill out your copyright notice in the Description page of Project Settings.

#include "M1A1Track.h"

UM1A1Track::UM1A1Track()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UM1A1Track::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction * ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void UM1A1Track::ApplySideForces()
{
	auto Slippagespeed = FVector::DotProduct(GetRightVector(), GetComponentVelocity());
	auto DeltaTime = GetWorld()->GetDeltaSeconds();
	auto As = (-Slippagespeed / DeltaTime)*GetRightVector();
	auto Tank = Cast<UStaticMeshComponent>(GetOwner()->GetRootComponent());
	auto F = (Tank->GetMass()*	As) / 2;

	Tank->AddForce(F);
}

void UM1A1Track::BeginPlay()
{
	Super::BeginPlay();
	OnComponentHit.AddDynamic(this, &UM1A1Track::onHit);
}

void UM1A1Track::onHit(UPrimitiveComponent * HitComponent, AActor * OtherActor, UPrimitiveComponent * OtherComp, FVector NormalImpulse, const FHitResult & Hit)
{
}

void UM1A1Track::OnDrive()
{
	auto ForceApplied = GetForwardVector()*TrackMaxdrivingForce*CurrentThrottle;
	auto Ltrack = GetSocketLocation("left");
	auto Rtrack= GetSocketLocation("Right");
	
	auto Tankroot = Cast<UPrimitiveComponent>(GetOwner()->GetRootComponent());
	Tankroot->AddForceAtLocation(ForceApplied, Ltrack);
	Tankroot->AddForceAtLocation(ForceApplied, Rtrack);
}

void UM1A1Track::SetTrack(float throttle)
{
}
