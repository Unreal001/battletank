// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include"Classes/Particles/ParticleSystemComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Classes/PhysicsEngine/RadialForceComponent.h"
#include "Classes/Kismet/GameplayStatics.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Projectile.generated.h"

UCLASS()
class BATTLETANK_API AProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AProjectile();

	UFUNCTION()
void	OnHit(UPrimitiveComponent * HitComponent, AActor * OtherActor, UPrimitiveComponent *OtherComp, FVector NormalImpulse, const FHitResult &Hit);
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	UProjectileMovementComponent *ProjectileMovement=nullptr;

	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent 		*CollisionMesh = nullptr;
	
	UPROPERTY(VisibleAnywhere)
     UParticleSystemComponent *LaunchBlast = nullptr;
	
	UPROPERTY(VisibleAnywhere)
	UParticleSystemComponent *ImpactBlast = nullptr;

	UPROPERTY(VisibleAnywhere)
	URadialForceComponent *ExplosionForce = nullptr;
	
	UPROPERTY(EditAnywhere)
	float DestroyTime=5.f;

	UPROPERTY(EditAnywhere)
		float ProjectileDamage= 10.f;
public:	
	// Called every frame
	UFUNCTION()
	void ProjectDestroy();

	void LunchProjectile(float LaunchSpeed);
	
};
