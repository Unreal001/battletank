// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/StaticMeshComponent.h"
#include "Track.generated.h"

/**
 * 
 */
UCLASS(meta = (BlueprintSpawnableComponent))
class BATTLETANK_API UTrack : public UStaticMeshComponent
{
	GENERATED_BODY()
	
	
private:

	UTrack();
	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	
	void ApplySideForces();
	
	virtual void BeginPlay() override;
	UFUNCTION()
	void onHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector  NormalImpulse, const FHitResult &Hit);

	
	

	void OnDrive();
	float CurrentThrottle = 0;

public:
	UPROPERTY(EditDefaultsOnly)
	float TrackMaxdrivingForce = 400000;

	UFUNCTION(BlueprintCallable, Category = InputSetup)
		void SetTrack(float throttle);
	
};
